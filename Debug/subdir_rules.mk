################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
main.obj: ../main.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"/home/plinio/ti/ccsv6/tools/compiler/msp430_4.3.3/bin/cl430" -vmspx --abi=eabi --code_model=large --data_model=restricted --near_data=none --include_path="/home/plinio/ti/ccsv6/ccs_base/msp430/include" --include_path="/home/plinio/ti/ccsv6/tools/compiler/msp430_4.3.3/include" --include_path="/home/plinio/workspace_v6_0/MSP430_Flash/driverlib/MSP430F5xx_6xx" --advice:power=all -g --define=__MSP430F5438__ --diag_warning=225 --display_error_number --diag_wrap=off --silicon_errata=CPU15 --silicon_errata=CPU18 --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="main.pp" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: $<'
	@echo ' '


