### Programa para escrita na memória flash do MSP430F5438 utilizando a biblioteca DRVLIB ###

O firmware reserva de 64Kb de flash (Bloco D inteiro do segmento de memória de código), apaga os 64Kb e finaliza com a escrita do número da iteração do contador de loop utilizado para escrita em cada posição de memória do segmento utilizado.