#include "driverlib.h"


/*
 * Pograma para escrita na memória flash do MSP430F5438 utilizando a biblioteca DRVLIB
 */

int main(void) {
	WDT_A_hold(WDT_A_BASE);
	/*
	 * Considerando que seja criada uma reserva de memoria de programa de 64Kb
	 * para uso como memória de informação a partir do endereço 0x30000 (Banco D inteiro da flash de programa).
	 * A reserva dessa mémoria foi feita especificando uma sessão de dados do usuario no arquivo lnk_msp430f5438.cmd,
	 * chamada FLASH_DADOS com inicio em 0x30000 e tamanho 0xffff(65536 bytes).
	 */


	uint8_t *Flash_ptr;				// Ponteiro para escrita na flash
	Flash_ptr = (uint8_t *)0x30000; // Desloca o ponteiro para o inicio do segmento desejado de memória



    FLASH_segmentErase(Flash_ptr); // Apaga o segmento que será utilizado para escrita dos dados

    unsigned int i;

    // Desligar as interrupções antes de qualquer escrita na flash !!!!
    for(i=0;i<0xffff;i++){	// Inicializa a escrita de 65536 bytes no segmento alocado para dados
    	FLASH_write16(&i,Flash_ptr+i, 1); // Escreve um dado de 16 bits (valor de i) no endereço apontado por Flash_ptr
    }
    // Religar as interrupções caso necessário ...


    while(1); // Fica mangueando.
}
